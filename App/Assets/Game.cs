using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;


public class Game : MonoBehaviour
{
    public GameObject typePersonToSpawn;
    private Dictionary<string, GameObject> d = new Dictionary<string, GameObject>();

    [System.Serializable]
    public class Persons
    {
        public Person[] persons;
    }

    [System.Serializable]
    public class Person
    {
        public string id;
        public float x;
        public float y;
        public int danger;
    }

    void Start()
    {
        StartCoroutine(tentimes());
    }

    IEnumerator tentimes()
    {
        for (int i = 0; i < 1000; i++) yield return StartCoroutine(Upload());
    }

    IEnumerator Upload()
    {
        yield return new WaitForSeconds(0.05f);
        WWWForm form = new WWWForm();
        form.AddField("myField", "myData");

        using (UnityWebRequest www = UnityWebRequest.Get("http://10.5.83.210:5000/objects"))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
            }
            else
            {
                string res = www.downloadHandler.text;
                Persons myObject = JsonUtility.FromJson<Persons>("{\"persons\":" + res + "}");
                foreach (Person person in myObject.persons)
                {
                    updatePerson(person);
                }
            }
        }
    }

    void updatePerson(Person p)
    {
        if (d.ContainsKey(p.id))
        {
            d[p.id].transform.position = new Vector3(p.x, p.y, 1.0f);
            if (p.danger > 0)
            {
                d[p.id].GetComponent<SpriteRenderer>().color = new Color(p.danger / 3.0f, 1 - p.danger / 3.0f, 0);
            }
            else
            {
                d[p.id].GetComponent<SpriteRenderer>().color = new Color(0, 1, 0);
            }
        }
        else
        {
            GameObject newPerson = Instantiate(typePersonToSpawn, new Vector3(p.x, p.y, 1.0f), Quaternion.identity);
            d.Add(p.id, newPerson);
        }
    }
}