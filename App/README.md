# Crowd Relief App


## Movement Detection
In 2010, Dirk Helbing and Anders Johansson wrote their Paper [Pedestrian, Crowd and Evacuation Dynamics](https://www.researchgate.net/publication/226065087_Pedestrian_Crowd_and_Evacuation_Dynamics) about changing movements of people in different sitations. This situation can for example be people in a crowd or even panicking.
<br>
We also did some experiments with our phones, to see whether we can observe such differences with our phones as well. 
Using the accelerometer (measure with 30Hz) of the phone we did some detection of different movements. One can see clear differences in the graphs. In the future one could also use such data to detect extremly crowded places. 


<br>
<a href="url"><img src="../../Documentation_Material/movement1.jpeg" align="left" height="33%" width="33%" ></a>
<a href="url"><img src="../../Documentation_Material/movement2.jpeg" align="center" height="33%" width="33%" ></a>
<a href="url"><img src="../../Documentation_Material/movement3.jpeg" align="right" height="33%" width="33%" ></a>
<br><br><br>

