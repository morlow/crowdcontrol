from matplotlib import pyplot as plt
import numpy as np
from scipy.fft import fft

'''
    Simple script to plot the Z-axis of the acceleration sensor in real-time
'''

def isHeatedMovement(arr):
	# Threshold has units of g
	accX = arr[:,0]
	accY = arr[:,1]
	accZ = arr[:,2]
	#accZ_ft = fft(accZ)[1:np.shape(arr)[1]//2]

	#if(np.max(np.abs(accZ_ft))>10):
	#	walking=True
	if(np.var(accX)>0.01 or np.var(accY) > 0.01):
		horizontalMovement = True
	#if not walking and horizontalMovement:
		return True

	return False

while True:
    accArr = np.loadtxt("data.csv",delimiter=",")
    plt.ylim([-2,1])
    plt.plot(accArr[:,2])
    plt.draw()
    plt.pause(0.001)
    plt.clf()
    if isHeatedMovement(accArr):
        print("Person being bumped")
    else:
        print()