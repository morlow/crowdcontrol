import math
import json
from random import random
from map import pos_valid, direction
from location import Location

class Person:
    def __init__(self, location, id, x,y):
        self.location = location
        self.id = id
        self.current_x = x
        self.current_y = y
        self.last_x = x
        self.last_y = y
        self.danger = 0
        self.location.grid[int(x)][int(y)].append(self)

    def get_direction(self):
        return self.current_x - self.last_x, self.current_y - self.last_y

    def update_pos(self, x, y, simulation=False):
        self.last_x = self.current_x
        self.last_y = self.current_y
        self.current_x = x
        self.current_y = y
        if int(self.last_x) != int(x) or int(self.last_y) != int(y):
            # remove self from self.location.grid[int(self.last_x)][int(self.last_y)]
            for i, person in enumerate(self.location.grid[int(self.last_x)][int(self.last_y)]):
                if person == self:
                    del self.location.grid[int(self.last_x)][int(self.last_y)][i]
                    break
            # append self to self.location.grid[int(x)][int(y)]
            self.location.grid[int(x)][int(y)].append(self)
        # Comment out to get back to working version
        self.get_risk_status()
            
    def generate_next_pos(self):
        v_x,v_y = self.get_direction()
        next_x = self.current_x + v_x
        next_y = self.current_y + v_y

        if random() < 0.2:
            dx, dy = direction[int(self.current_x), int(self.current_y)]
            if abs(dx) > abs(dy):
                x = self.current_x + dx/20
                y = self.current_y + (random()-0.5)/10.0
            elif abs(dy) > abs(dx):
                x = self.current_x + (random()-0.5)/10.0
                y = self.current_y + dy/20
            else:
                x = self.current_x + (random()-0.5)/5.0
                y = self.current_y + (random()-0.5)/5.0
                while pos_valid[int(x),int(y)] == 0:
                    x = self.current_x + (random()-0.5)/5.0
                    y = self.current_y + (random()-0.5)/5.0
        else:
            if v_x == 0 and v_y == 0 or random() < 0.2 or pos_valid[int(next_x),int(next_y)] == 0:
                x = self.current_x + (random()-0.5)/5.0
                y = self.current_y + (random()-0.5)/5.0
                while pos_valid[int(x),int(y)] == 0:
                    x = self.current_x + (random()-0.5)/5.0
                    y = self.current_y + (random()-0.5)/5.0
            
            else:
                x = self.current_x + v_x
                y = self.current_y + v_y
        return x,y


    def simulate_pos(self):
        x,y = self.generate_next_pos()
        if self.is_colliding(x,y):
            self.update_pos(self.current_x, self.current_y)
        else:
            self.update_pos(x,y)


    def is_colliding(self, x,y):
        grid = self.location.grid
        i,j = int(x),int(y)
        persons = grid[i][j]
        persons = [person for person in persons if isinstance(person,Person)]
        for person in persons:
            if person != self:
                d = math.sqrt(pow(person.current_x - x,2)+pow(person.current_y-y,2))
                if d < 0.5:
                    return True
        return False


    def get_risk_status(self):
        if self.location.density_is_too_high(self, self.current_x, self.current_y):
            return "DANGER"
        return "Everything fine"

    def to_json(self):
        return {"id": self.id, "x": self.current_x, "y": self.current_y, "danger": self.danger}
        