# Server
The server receives the position of all participants and calculates the people density. It detects critical areas and informs the corresponding persons

## Test Simulation
Since the server can't be tested in real life, we did some crowd simulations. In the following Image one can see a screenshot of such a simulation. Different collor show wheter the situation could get dangerous at this place. 
<br><br><br>
<a href="url"><img src="../Documentation_Material/simulation.jpeg" align="center" height="30%" width="30%" ></a>
<br><br><br><br><br><br><br>
.
