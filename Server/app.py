import imp
from random import uniform
from flask import Flask, redirect, url_for, request, jsonify
from location import Location
from person import Person
from map import pos_valid
import numpy as np
from matplotlib import pyplot as plt
import pandas as pd

app = Flask(__name__)

UNIQUE_ID = 0

location = Location(1.5, 50) # max_density, max_distance

# Create Sample persons, (200, 1.5), (500, 5)
number_of_persons = 200
persons = []
for i in range(number_of_persons):
    x = uniform(0,20);
    y = uniform(0,20);
    while pos_valid[int(x),int(y)] == 0:
        x = uniform(0,20);
        y = uniform(0,20);
    location.persons.append(Person(location, len(location.persons), x, y))


# Sent from Android App to register user and get ID
@app.route("/register", methods=['POST'])
def person_id():
    x = request.values['latitude']
    y = request.values['longitude']
    id = len(location.persons)
    location.persons.append(Person(location, id, x, y))
    print(f"ID: {id}")
    return str(id)


# Sent from Android App to update position
@app.route("/position", methods=['POST'])
def post_position():
    print("Position received")
    id = int(request.values['id'])
    print(f"ID: {id}")
    x = float(request.values['latitude'])
    y = float(request.values['longitude'])
    print(f"id: {id}, x: {x}, y: {y}")
    acc_vec = str(request.values['accelerationVector'])
    print(acc_vec)

    with open('data.csv','a') as fd:
        fd.write(acc_vec)
    with open('data.csv', 'r') as fd:
        lines = fd.readlines()
    with open('data.csv', 'w') as fd:
        fd.writelines(lines[30:])
    #location.persons[id].update_pos(x,y)
    return "Danger"
    return location.persons[id].get_risk_status(location.max_distance)


# Sent from Android App (right now from Unity Simulation Project) to receive the position of all persons
@app.route("/objects", methods=['GET'])
def send_objects():
    print("Simulate_pos")
    # Simulate movement
    for person in location.persons:
        person.simulate_pos()
    print("End simulation")
    return jsonify(location.to_json())
