import numpy as np
from map import pos_valid, direction, width

class Location:
    def __init__(self, max_density, max_distance, persons=[]):
        self.max_density = max_density
        self.max_distance = max_distance
        self.persons = persons
        self.grid = []
        self.grid = []
        for i in range(21):
            self.grid.append([])
            for j in range(21):
                self.grid[i].append([])

    def density_is_too_high(self,person, x, y):
        # Compute density
        width = 0
        persons = 0
        i,j = int(x),int(y)
        dx,dy = direction[i,j]
        if dx > dy:
            k = j
            while pos_valid[i,k] == 1:
                persons += len(self.grid[i][k])
                width += 1
                k += 1
            k = j-1
            while pos_valid[i,k] == 1:
                persons += len(self.grid[i][k])
                width += 1
                k -= 1
        else:
            k = i
            while pos_valid[k,j] == 1:
                persons += len(self.grid[k][j])
                width += 1
                k += 1
            k = i-1
            while pos_valid[k,j] == 1:
                persons += len(self.grid[k][j])
                width += 1
                k -= 1
        width = 1 if width == 0 else width
        density = persons / width

        print(f"Density at x={x}, y={y} is {density}")
        person.danger = density
        return density >= self.max_density  

    def to_json(self):
        return [person.to_json() for person in self.persons]