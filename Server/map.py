import numpy as np

pos_valid = np.zeros((21,21), dtype=int)
width = np.zeros((21,21), dtype='2i')


# Specify valid points in Map1 (Unity)
for x in range(1,20):
    for y in range(1,6):
        pos_valid[x,y] = 1
    for y in range(14,20):
        pos_valid[x,y] = 1

for x in range(8,12):
    for y in range(1,20):
        pos_valid[x,y] = 1


# Compute the direction where the wall is the mot far away
direction = np.zeros((21,21), dtype='2i')

for i in range(21):
    for j in range(21):
        l,t,r,b = 0,0,0,0
        if pos_valid[i,j] == 1:
            c = 0
            while pos_valid[i+c,j] == 1:
                c+=1
            r = c
            c=0
            while pos_valid[i-c,j] == 1:
                c+=1
            l = c
            c=0
            while pos_valid[i,j+c] == 1:
                c+=1
            t = c
            c = 0
            while pos_valid[i,j-c] == 1:
                c+=1
            b = c
            
            dx,dy = (0,0)
            if r > l:
                dx = 1
            elif l > r:
                dx = -1
            if t > b:
                dy = 1
            elif b > t:
                dy = -1
            if max(r,l) > max(t,b):
                dx *= 2
            elif max(t,b) > max(r,l):
                dy *= 2
            
            width[i,j] = l+r, t+b
            
            direction[i,j] = dx,dy


            
