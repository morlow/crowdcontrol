#The function map_to_array opens the image (map.png) in the parent directory and crops it (0,0,n,n). It creats a bitmap of the the resulting bitmap and return this bit map in a nxn np-array

import numpy as np
from PIL import Image

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

#PRE: The function takes a value n (default n=100 for no input), which gives the size of the nxn array one wants to create. This file has to be in the same folder as the map, map.png which will be imported by the program.  
#POST: It returns an np-array of size nxn which contains a bitmap of the cropped(0,0,n,n) input

def map_to_array(n=100):
   #open image
   image = Image.open('map.png')

   #crop image
   box = (0, 0, n, n)
   cropped_image = image.crop(box)
   
   #make a greyscale image out of color image
   greyscale_image = cropped_image.convert('L')

   # Threshold PIL grayscale image using point with threshold which needs to be adapted depending on the colors of the map.
   threshold = 250
   image = greyscale_image.point(lambda p: p > threshold and 255)

   ASCII_BITS = '0', '1'

   #convert image to bitmap
   image = image.convert('1')

   width, height = image.size
   #image.show()
   # Convert the image data to a list of ASCII bits.
   data = [ASCII_BITS[bool(val)] for val in image.getdata()]
   # Convert data to 2D list (list of bits)
   data = [data[offset: offset+width] for offset in range(0, width*height, width)]
   map_array = np.array(data)
   return map_array


#np.set_printoptions(threshold=np.inf) #print whole array and not only truncated one to terminal
print(map_to_array(100))
print('fini')


