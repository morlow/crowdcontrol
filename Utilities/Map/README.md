# Generating Maps
This function should help to generate bitmap arrays out of maps. This should give the server the possibility to work with the map and recongize obstacles inside the scene and boundaries of the scene.
The threshold needs to be adapted depending on the colors of the map. 
Having things written on the map, leads to errors because the written things will be interpreted as an obstacle. 

## Used Libraries
- numpy
- Pillow Image

## Examples
The examples show the edited pictures which then will be transformed into the numpy array.
### Labyrinth 
original: <br>
<a href="url"><img src="../../Documentation_Material/Labyrinth.png" align="center" height="20%" width="20%" ></a>
<br>
<br>
after Python edit (threshold=180, n=100) <br>
<a href="url"><img src="../../Documentation_Material/labedit.PNG" align="center" height="10%" width="10%" ></a>
<br>
<br>
<br>
### Map
original: <br>
<a href="url"><img src="../../Documentation_Material/map.jpg" align="center" height="40%" width="40%" ></a>
<br>
<br>
after Python edit (threshold=250, n=100) <br>
<a href="url"><img src="../../Documentation_Material/map_ed.PNG" align="center" height="20%" width="20%" ></a> <br><br><br>
after Python edit (threshold=250, n=700) <br>
<a href="url"><img src="../../Documentation_Material/map_ed700.PNG" align="center" height="20%" width="20%" ></a>
<br>
