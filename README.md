# Crowd Relief 
<!---Change Name of Projet!!!!!!! -->
Crowd Relief is an app, that helps organizers make big events safer. It notifies you, when you move towards a crowd which has the potential to lead to a mass panic, and gives you a proposal of a better direction to go. With the app, people can help to dissolve a dangerous crowd instead of aggravating the situation. 

<a href="url"><img src="./Documentation_Material/crowdRelief.png" align="right" height="30%" width="30%" ></a>
<!---short summary -->
## Download and usage
To use the code as a base for further implementations, the whole code can be downloaded as follows:<br>
without ssh key<br>
`git clone https://gitlab.ethz.ch/morlow/crowdcontrol.git` <br>
or with ssh key<br>
`git clone git@gitlab.ethz.ch:morlow/crowdcontrol.git`


### [The App](./App)
It is an Android app. 

To adapt the App donwload the App folder containing all the files of the Unity project. Open the folder ["App"](./App) with Unity for editing. For a documentation on how to open and edit the Unity files we refer to the [Unity documentation](https://docs.unity3d.com/Manual/index.html).

### [The Server](./Server)

### Crowd simulation

## Goal and Motivation
<!---![parade](./Documentation_Material/parade.jpg)-->
<a href="url"><img src="./Documentation_Material/parade.png" align="left" height="30%" width="30%" ></a>

This project was developed in summer 2022 during the Block Course ["Participatory Resilience"](https://participatoryresilience.ch/) at ETH, which is organized as a Hackathon. The goal is to make large events more resilient against mass panics using the position of the participants. With the help of the app, the participants can contribute to safety by avoiding very crowded areas, which they might have underestimated without the information of the app.  
Sadly, mass panic is not a isolated event, but can happen at any big event with a lot of people and the consequences are dramatic. Therefore it has to be avoided at any cost. To make such events safer for everyone, everyone can contribute by making the right decisions. To make these decisions, the app detects dangerous places where there are already a lot of people in a narrow space and tells the users in which direction to go as not make the situation worse.  

<br>
<br>
<br>

## Code Structure
![sequence](./Documentation_Material/Sequence.png)
### [App](./App)
The android App is implemented using [Unity (2021.3.9f1)](https://docs.unity3d.com/Manual/index.html) in C#.
<a href="url"><img src="./Documentation_Material/phone.png" align="right" height="20%" width="20%" ></a>

<br>
<br>
<br>

#### Detection of movement
By plotting the movement of the phone/person we could observe clear differences in the plot for normal walking and staying more or less at the same position while beeing pushed around. The movement of someone in a crowded area differs clearly from one that just walks freely, if one can detect that, this might also be a possibility to identify wether there are potentially dangerous crowds. The Plots can be seen in the App folder.

### [Server](./Server)
The Server is implented in Python. And is responsible for calculating all people densities. It detects, where there might be areas to avoid and advises people, which are going in this direction to change their direction. 

#### [Map editing](./Utilities/Map)
The server uses an np-array, containing a bit-map of the map to determine wheter there is an obstacle somewhere or wheter a person can move to this position, such that the app doesn't tell someone to walk into a direction in which they can't move. To create such a bit-map we used a python function.


### Simulation of people movement
The simulation of moving people is not included in the end product, but it helps to test the implementations and to see how changing behaviour of people leads to different end results. 


## Outlook
Due to limited time, there are still a few things that have to be done, before using the App for an event
- Publish the user app to the App Store and the Google Play Store
- Test functionality in real world situations
- ....

## Sources
Sources of pictures shown here are in the [Documentation_material](./Documentation_Material) folder
